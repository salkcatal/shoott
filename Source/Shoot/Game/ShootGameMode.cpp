// Copyright Epic Games, Inc. All Rights Reserved.

#include "ShootGameMode.h"
#include "ShootPlayerController.h"
#include "Character/ShootCharacter.h"
#include "UObject/ConstructorHelpers.h"

AShootGameMode::AShootGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AShootPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}